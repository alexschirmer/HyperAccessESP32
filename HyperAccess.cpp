#include <WiFi.h>
#include <IOXhop_FirebaseESP32.h>
#include <time.h>

#include <MFRC522.h> //biblioteca responsável pela comunicação com o módulo RFID-RC522
#include <SPI.h>     //biblioteca para comunicação do barramento SPI



#define SS_PIN 21
#define RST_PIN 22

#define SIZE_BUFFER 18
#define MAX_SIZE_BLOCK 16



//===================================================
// FIREBASE CONFIG

#define FIREBASE_HOST "hyperacess.firebaseio.com"
#define FIREBASE_AUTH "Cv1DhTlc6cDq1p62AA2gBoodjnol99DO2nXLkT4w"
#define WIFI_SSID "CondomerWiFi"
#define WIFI_PASSWORD "condomerasfv"

//---------------------------------------------------


//#include <SoftwareSerial.h>
#include "FPM.h"

// Search the DB for a print with this example

// pin #2 is IN from sensor (GREEN wire)
// pin #3 is OUT from arduino  (WHITE/YELLOW wire)
//SoftwareSerial mySerial(2, 3);

// pin #16 is IN from sensor (GREEN wire)
// pin #17 is OUT from arduino  (WHITE/YELLOW wire)
HardwareSerial Serial2(2);
FPM finger;

//===================================================
//esse objeto 'chave' é utilizado para autenticação
MFRC522::MIFARE_Key key;
//código de status de retorno da autenticação
MFRC522::StatusCode status;

// Definicoes pino modulo RC522
MFRC522 mfrc522(SS_PIN, RST_PIN);

//

#define GREEN_LED 27
#define RED_LED 26 
int count0 = 0;
int timezone = -3 * 3600;
int dst = 0;
String listCardsBD;
String listBiometricBD;
JsonVariant teste;
// define cores
static uint8_t taskCoreZero = 0;
static uint8_t taskCoreOne = 1;

void openDoor()
{

  digitalWrite(GREEN_LED, HIGH);
  Serial.println("Sinal Alto para abrir porta");

  if (ackOpenDoor())
  {
    delay(600);
    digitalWrite(GREEN_LED, LOW);
    Serial.println("Sinal Baixo para abrir porta");
  }
}

boolean ackOpenDoor()
{
  // set value
  Firebase.setBool("/command/opened", true);
  // handle error
  if (Firebase.failed())
  {
    Serial.print("setting /command/opened failed:");
    Serial.println(Firebase.error());
    return false;
  }
  else
    return true;
}
void notAuth(){
  digitalWrite(RED_LED, HIGH);
  delay(250);
  digitalWrite(RED_LED, LOW);
  delay(250);
  digitalWrite(RED_LED, HIGH);
  delay(250);
  digitalWrite(RED_LED, LOW);
  delay(250);
  digitalWrite(RED_LED, HIGH);
  delay(250);
  digitalWrite(RED_LED, LOW);
  delay(250);

}


int getFingerprintID() {
  int p = -1;
  Serial.println("Waiting for a finger...");
  while (p != FINGERPRINT_OK){
    p = finger.getImage();
    switch (p) {
      case FINGERPRINT_OK:
        Serial.println("Image taken");
        break;
      case FINGERPRINT_NOFINGER:
       // Serial.print(".");
        break;
      case FINGERPRINT_PACKETRECIEVEERR:
        Serial.println("Communication error");
        break;
      case FINGERPRINT_IMAGEFAIL:
        Serial.println("Imaging error");
        break;
      default:
        Serial.println("Unknown error");
        break;
    }
    yield();
  }

  // OK success!

  p = finger.image2Tz();
  switch (p) {
    case FINGERPRINT_OK:
      Serial.println("Image converted");
      break;
    case FINGERPRINT_IMAGEMESS:
      Serial.println("Image too messy");
      return p;
    case FINGERPRINT_PACKETRECIEVEERR:
      Serial.println("Communication error");
      return p;
    case FINGERPRINT_FEATUREFAIL:
      Serial.println("Could not find fingerprint features");
      return p;
    case FINGERPRINT_INVALIDIMAGE:
      Serial.println("Could not find fingerprint features");
      return p;
    default:
      Serial.println("Unknown error");
      return p;
  }

  Serial.println("Remove finger...");
  while (p != FINGERPRINT_NOFINGER){
    p = finger.getImage();
    yield();
  }
  Serial.println();
  // OK converted!
  p = finger.fingerFastSearch();
  if (p == FINGERPRINT_OK) {
    Serial.println("Found a print match!");
  } else if (p == FINGERPRINT_PACKETRECIEVEERR) {
    Serial.println("Communication error");
    return p;
  } else if (p == FINGERPRINT_NOTFOUND) {
    Serial.println("Did not find a match");
    notAuth();
    return p;
  } else {
    Serial.println("Unknown error");
    return p;
  }   
  
  // found a match!
  Serial.print("Found ID #"); Serial.print(finger.fingerID); 
  Serial.print(" with confidence of "); Serial.println(finger.confidence);

  findBiometric(String(finger.fingerID)); 
  return p;
}
void findBiometric(String fingerID){
  if (strstr(listBiometricBD.c_str(), fingerID.c_str()))
  {
    openDoor();
    insertLog2(fingerID, "Biometria");
  } else {
    notAuth();
  }
}

bool insertLog2(String fingerID, String type)
{
  
  time_t now = time(nullptr);
  String time = String(now);
  time = time + "000";

  String userID = Firebase.getString("usersBiometric/" + fingerID);

  StaticJsonBuffer<200> jsonBuffer;
  JsonObject &object = jsonBuffer.createObject();
  object["timestamp"] = time;
  object["type"] = type;
  JsonVariant variant = object;

  Firebase.push("logs/" + userID, variant);
   Firebase.setBool("/command/opened", false);
  // handle error
  if (Firebase.failed())
  {
    Serial.print("setting /command/opened failed:");
    Serial.println(Firebase.error());
    return false;
  }
  else
    return true;


  Serial.println(now);
}



void findCard(unsigned long cardID)
{
  String strID = String(cardID);

  if (strstr(listCardsBD.c_str(), strID.c_str()))
  {
    openDoor();
    insertLog(cardID, "Cartão RFID");
  } else {
    notAuth();
  }
}

bool insertLog(unsigned long cardID, String type)
{
  String strCardID = String(cardID);
  time_t now = time(nullptr);
  String time = String(now);
  time = time + "000";

  String userID = Firebase.getString("usersCards/" + strCardID);

  StaticJsonBuffer<200> jsonBuffer;
  JsonObject &object = jsonBuffer.createObject();
  object["timestamp"] = time;
  object["type"] = type;
  JsonVariant variant = object;

  Firebase.push("logs/" + userID, variant);
   Firebase.setBool("/command/opened", false);
  // handle error
  if (Firebase.failed())
  {
    Serial.print("setting /command/opened failed:");
    Serial.println(Firebase.error());
    return false;
  }
  else
    return true;


  Serial.println(now);
}

//======================================== TASKS CREATE

//essa função será responsável apenas por atualizar as informações no
//display a cada 100ms
void coreTaskZero(void *pvParameters)
{
  while (true)
  {
    if (mfrc522.PICC_IsNewCardPresent())
    {
      
      unsigned long uid = getCardID();
      if (uid != -1)
      {
        Serial.print("Card detected, UID: ");
        Serial.println(uid);
        Serial.println("Buscando no Banco...");
        findCard(uid);
      }
    }
    delay(10);
  }
}

unsigned long getCardID()
{
  if (!mfrc522.PICC_ReadCardSerial())
  { //Since a PICC placed get Serial and continue
    return -1;
  }
  unsigned long hex_num;
  hex_num = mfrc522.uid.uidByte[0] << 24;
  hex_num += mfrc522.uid.uidByte[1] << 16;
  hex_num += mfrc522.uid.uidByte[2] << 8;
  hex_num += mfrc522.uid.uidByte[3];
  mfrc522.PICC_HaltA(); // Stop reading
  mfrc522.PCD_StopCrypto1();
  return hex_num;
}

void setup()
{

  pinMode(GREEN_LED, OUTPUT);
  pinMode(RED_LED, OUTPUT);
  Serial.begin(115200);
  SPI.begin(); // Init SPI bus
  mfrc522.PCD_Init();
  
  Serial2.begin(57600);
  
  if (finger.begin(&Serial2)) {
    Serial.println("Found fingerprint sensor!");
    Serial.print("Capacity: "); Serial.println(finger.capacity);
    Serial.print("Packet length: "); Serial.println(finger.packetLen);
  } else {
    Serial.println("Did not find fingerprint sensor :(");
    while (1) yield();
  }
  // connect to wifi.
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  Serial.print("connecting");
  while (WiFi.status() != WL_CONNECTED)
  {
    Serial.print(".");
    delay(500);
  }
  Serial.println();
  Serial.print("connected: ");
  Serial.println(WiFi.localIP());

  Firebase.begin(FIREBASE_HOST, FIREBASE_AUTH);

  //Stream create of open door command

  Firebase.stream("/command/open", [](FirebaseStream stream) {
    if (stream.getEvent() == "put" /*&& stream.getPath() == "/command/open"*/)
    {
      if (stream.getDataBool())
        openDoor();
    }
  });

  listCardsBD = Firebase.getString("listCards");
  listBiometricBD = Firebase.getString("listBiometric");

  configTime(timezone, dst, "pool.ntp.org", "time.nist.gov");
  Serial.println("\nWaiting for Internet time");

  while (!time(nullptr))
  {
    Serial.print("*");
    delay(1000);
  }
  Serial.println("\nTime response....OK");

  //================================================
  // TASKS CREATE
  xTaskCreatePinnedToCore(
      coreTaskZero,   /* função que implementa a tarefa */
      "coreTaskZero", /* nome da tarefa */
      10000,          /* número de palavras a serem alocadas para uso com a pilha da tarefa */
      NULL,           /* parâmetro de entrada para a tarefa (pode ser NULL) */
      1,              /* prioridade da tarefa (0 a N) */
      NULL,           /* referência para a tarefa (pode ser NULL) */
      taskCoreOne);   /* Núcleo que executará a tarefa */

  delay(500); //tempo para a tarefa iniciar
}
int n = 0;
//char myString[] = "This is the first line";

void loop()
{

  getFingerprintID();
}
